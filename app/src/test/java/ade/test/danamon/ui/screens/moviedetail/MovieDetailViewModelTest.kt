package ade.test.mandiri.ui.screens.moviedetail

import ade.test.mandiri.MainDispatcherRule
import ade.test.mandiri.TestDataClassGenerator
import ade.test.mandiri.data.repository.MovieRepository
import ade.test.mandiri.ui.screens.mainscreen.MainViewModel
import ade.test.mandiri.utils.network.DataState
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Exception

class MovieDetailViewModelTest {

    private lateinit var vm: MovieDetailViewModel

    lateinit var movieRepository: MovieRepository

    private val testDataClassGenerator: TestDataClassGenerator = TestDataClassGenerator()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        movieRepository = mockk(relaxUnitFun = true)
        vm = MovieDetailViewModel(movieRepository)
    }

    @Test
    fun `movieDetailApi, success response, data valid`() = runTest {
        // GIVEN
        val dummyMovieDetail = DataState.Success(testDataClassGenerator.getMovieDetailResponse())
        val dummyMovieDetailFlow = flow {
            emit(dummyMovieDetail)
        }
        coEvery { movieRepository.movieDetail(any()) } returns dummyMovieDetailFlow

        // WHEN
        vm.movieDetailApi(123)

        // THEN
        assertEquals(vm.movieDetail.value, dummyMovieDetail)
    }

    @Test
    fun `movieDetailApi, error response, data valid`() = runTest {
        // GIVEN
        val dummyMovieDetail = DataState.Error(Exception("Some Error"))
        val dummyMovieDetailFlow = flow {
            emit(dummyMovieDetail)
        }
        coEvery { movieRepository.movieDetail(any()) } returns dummyMovieDetailFlow

        // WHEN
        vm.movieDetailApi(123)

        // THEN
        assertEquals(vm.movieDetail.value, dummyMovieDetail)
    }

    @Test
    fun `movieDetailApi, loading response, data valid`() = runTest {
        // GIVEN
        val dummyMovieDetail = DataState.Loading
        val dummyMovieDetailFlow = flow {
            emit(dummyMovieDetail)
        }
        coEvery { movieRepository.movieDetail(any()) } returns dummyMovieDetailFlow

        // WHEN
        vm.movieDetailApi(123)

        // THEN
        assertEquals(vm.movieDetail.value, dummyMovieDetail)
    }

    @Test
    fun `recommendedMovieApi, success response, data valid`() = runTest {
        // GIVEN
        val dummyMovieRecommendation = DataState.Success(testDataClassGenerator.getMovieRecommendationResponse())
        val dummyMovieRecommendationFlow = flow {
            emit(dummyMovieRecommendation)
        }
        coEvery { movieRepository.recommendedMovie(any(), any()) } returns dummyMovieRecommendationFlow

        // WHEN
        vm.recommendedMovieApi(123, 1)

        // THEN
        assertEquals(vm.recommendedMovie.value, dummyMovieRecommendation)
    }

    @Test
    fun `recommendedMovieApi, error response, data valid`() = runTest {
        // GIVEN
        val dummyMovieRecommendation = DataState.Error(Exception("Some Error"))
        val dummyMovieRecommendationFlow = flow {
            emit(dummyMovieRecommendation)
        }
        coEvery { movieRepository.recommendedMovie(any(), any()) } returns dummyMovieRecommendationFlow

        // WHEN
        vm.recommendedMovieApi(123, 1)

        // THEN
        assertEquals(vm.recommendedMovie.value, dummyMovieRecommendation)
    }

    @Test
    fun `recommendedMovieApi, loading response, data valid`() = runTest {
        // GIVEN
        val dummyMovieRecommendation = DataState.Loading
        val dummyMovieRecommendationFlow = flow {
            emit(dummyMovieRecommendation)
        }
        coEvery { movieRepository.recommendedMovie(any(), any()) } returns dummyMovieRecommendationFlow

        // WHEN
        vm.recommendedMovieApi(123, 1)

        // THEN
        assertEquals(vm.recommendedMovie.value, dummyMovieRecommendation)
    }

    @Test
    fun `movieCredit, success response, data valid`() = runTest {
        // GIVEN
        val dummyMovieCredit = DataState.Success(testDataClassGenerator.getMovieCreditResponse())
        val dummyMovieCreditFlow = flow {
            emit(dummyMovieCredit)
        }
        coEvery { movieRepository.movieCredit(any()) } returns dummyMovieCreditFlow

        // WHEN
        vm.movieCredit(123)

        // THEN
        assertEquals(vm.artist.value, dummyMovieCredit)
    }

    @Test
    fun `movieCredit, error response, data valid`() = runTest {
        // GIVEN
        val dummyMovieCredit = DataState.Error(Exception("Some Error"))
        val dummyMovieCreditFlow = flow {
            emit(dummyMovieCredit)
        }
        coEvery { movieRepository.movieCredit(any()) } returns dummyMovieCreditFlow

        // WHEN
        vm.movieCredit(123)

        // THEN
        assertEquals(vm.artist.value, dummyMovieCredit)
    }

    @Test
    fun `movieCredit, loading response, data valid`() = runTest {
        // GIVEN
        val dummyMovieCredit = DataState.Loading
        val dummyMovieCreditFlow = flow {
            emit(dummyMovieCredit)
        }
        coEvery { movieRepository.movieCredit(any()) } returns dummyMovieCreditFlow

        // WHEN
        vm.movieCredit(123)

        // THEN
        assertEquals(vm.artist.value, dummyMovieCredit)
    }

    @Test
    fun `movieVideo, success response, data valid`() = runTest {
        // GIVEN
        val dummyMovieVideo = DataState.Success(testDataClassGenerator.getMovieVideoResponse())
        val dummyMovieVideoFlow = flow {
            emit(dummyMovieVideo)
        }
        coEvery { movieRepository.movieVideo(any()) } returns dummyMovieVideoFlow

        // WHEN
        vm.movieVideo(123)

        // THEN
        assertEquals(vm.movieVideos.value, dummyMovieVideo)
    }

    @Test
    fun `movieVideo, error response, data valid`() = runTest {
        // GIVEN
        val dummyMovieVideo = DataState.Error(Exception("Some Error"))
        val dummyMovieVideoFlow = flow {
            emit(dummyMovieVideo)
        }
        coEvery { movieRepository.movieVideo(any()) } returns dummyMovieVideoFlow

        // WHEN
        vm.movieVideo(123)

        // THEN
        assertEquals(vm.movieVideos.value, dummyMovieVideo)
    }

    @Test
    fun `movieVideo, loading response, data valid`() = runTest {
        // GIVEN
        val dummyMovieVideo = DataState.Loading
        val dummyMovieVideoFlow = flow {
            emit(dummyMovieVideo)
        }
        coEvery { movieRepository.movieVideo(any()) } returns dummyMovieVideoFlow

        // WHEN
        vm.movieVideo(123)

        // THEN
        assertEquals(vm.movieVideos.value, dummyMovieVideo)
    }
}