package ade.test.mandiri.ui.screens.mainscreen

import ade.test.mandiri.MainDispatcherRule
import ade.test.mandiri.TestDataClassGenerator
import ade.test.mandiri.data.repository.MovieRepository
import ade.test.mandiri.utils.network.DataState
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.time.delay
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Exception
import java.time.Duration

class MainViewModelTest {

    private lateinit var vm: MainViewModel

    lateinit var movieRepository: MovieRepository

    private val testDataClassGenerator: TestDataClassGenerator = TestDataClassGenerator()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        movieRepository = mockk(relaxUnitFun = true)
        vm = MainViewModel(movieRepository)
    }

    @Test
    fun `genreList, success response, data valid`() = runTest {
        // GIVEN
        val dummyGenre = DataState.Success(testDataClassGenerator.getGenreMovieList())
        val dummyGenreFlow = flow {
            emit(dummyGenre)
        }
        coEvery { movieRepository.genreList() } returns dummyGenreFlow

        // WHEN
        vm.genreList()

        // THEN
        assertEquals(vm.genres.value, dummyGenre)
    }

    @Test
    fun `genreList, error response, data valid`() = runTest {
        // GIVEN
        val dummyGenre = DataState.Error(Exception("Some Error"))
        val dummyGenreFlow = flow {
            emit(dummyGenre)
        }
        coEvery { movieRepository.genreList() } returns dummyGenreFlow

        // WHEN
        vm.genreList()

        // THEN
        assertEquals(vm.genres.value, dummyGenre)
    }

    @Test
    fun `genreList, loading response, data valid`() = runTest {
        // GIVEN
        val dummyGenre = DataState.Loading
        val dummyGenreFlow = flow {
            emit(dummyGenre)
        }
        coEvery { movieRepository.genreList() } returns dummyGenreFlow

        // WHEN
        vm.genreList()

        // THEN
        assertEquals(vm.genres.value, dummyGenre)
    }

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    @Test
    fun `searchApi, success response, data valid`() = runTest {
        // GIVEN
        val dummySearch = DataState.Success(testDataClassGenerator.getSearchResponse())
        val dummySearchFlow = flow {
            emit(dummySearch)
        }
        coEvery { movieRepository.search(any()) } returns dummySearchFlow

        // WHEN
        vm.searchApi("asd")

        // THEN
        assertEquals(vm.searchData.value, dummySearch)
    }

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    @Test
    fun `searchApi, error response, data valid`() = runTest {
        // GIVEN
        val dummySearch = DataState.Error(Exception("Some Error"))
        val dummySearchFlow = flow {
            emit(dummySearch)
        }
        coEvery { movieRepository.search(any()) } returns dummySearchFlow

        // WHEN
        vm.searchApi("asd")

        // THEN
        assertEquals(vm.searchData.value, dummySearch)
    }

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    @Test
    fun `searchApi, loading response, data valid`() = runTest {
        // GIVEN
        val dummySearch = DataState.Loading
        val dummySearchFlow = flow {
            emit(dummySearch)
        }
        coEvery { movieRepository.search(any()) } returns dummySearchFlow

        // WHEN
        vm.searchApi("asd")

        // THEN
        assertEquals(vm.searchData.value, dummySearch)
    }
}