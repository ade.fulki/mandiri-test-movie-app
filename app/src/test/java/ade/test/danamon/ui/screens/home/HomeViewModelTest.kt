package ade.test.mandiri.ui.screens.home

import ade.test.mandiri.MainDispatcherRule
import ade.test.mandiri.TestDataClassGenerator
import ade.test.mandiri.data.datasource.remote.paging.NowPlayingPagingDataSource
import ade.test.mandiri.data.model.GenreId
import ade.test.mandiri.data.model.moviedetail.Genre
import ade.test.mandiri.data.repository.MovieRepository
import ade.test.mandiri.ui.screens.mainscreen.MainViewModel
import ade.test.mandiri.utils.network.DataState
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HomeViewModelTest {

    private lateinit var vm: HomeViewModel

    lateinit var movieRepository: MovieRepository

    private val testDataClassGenerator: TestDataClassGenerator = TestDataClassGenerator()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        movieRepository = mockk(relaxUnitFun = true)
        vm = HomeViewModel(movieRepository)
    }

    @Test
    fun `nowPlayingMovies, success response, data valid`() = runTest {
        // GIVEN
        val dummyNowPlaying = PagingData.from(testDataClassGenerator.getNowPlayingResponse().results)
        val dummyNowPlayingFlow = flow { emit(dummyNowPlaying) }

        coEvery { movieRepository.nowPlayingPagingDataSource(any()) } returns dummyNowPlayingFlow

        // WHEN
        val result = vm.nowPlayingMovies.first()

        // THEN
        assertEquals(result, dummyNowPlaying)
    }

    @Test
    fun `selectedGenre, set value, data valid`() = runTest {
        // GIVEN
        val dummyGenre = Genre(1, "")

        // WHEN
        vm.selectedGenre.value = dummyGenre

        // THEN
        assertEquals(vm.selectedGenre.value, dummyGenre)
    }

    @Test
    fun `filterData, set value, data valid`() = runTest {
        // GIVEN
        val dummyGenreId = GenreId( "id")

        // WHEN
        vm.filterData.value = dummyGenreId

        // THEN
        assertEquals(vm.filterData.value, dummyGenreId)
    }
}