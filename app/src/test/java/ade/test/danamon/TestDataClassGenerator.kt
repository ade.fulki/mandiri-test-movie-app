package ade.test.mandiri

import ade.test.mandiri.data.model.BaseModel
import ade.test.mandiri.data.model.Genres
import ade.test.mandiri.data.model.artist.Artist
import ade.test.mandiri.data.model.moviedetail.MovieDetail
import ade.test.mandiri.data.model.review.Review
import ade.test.mandiri.data.model.video.Video
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File

class TestDataClassGenerator {

    private val gson = Gson()

    private inline fun <reified T> buildDataClassFromJson(json: String): T {
        val typeToken = object : TypeToken<T>() {}.type
        return gson.fromJson(json, typeToken)
    }

    fun getGenreMovieList(): Genres {
        val jsonString = getJson("genre-movie-list.json")
        return buildDataClassFromJson(jsonString)
    }


    fun getMovieDetailResponse(): MovieDetail {
        val jsonString = getJson("movie-detail.json")
        return buildDataClassFromJson(jsonString)
    }

    fun getMovieReviewResponse(): Review {
        val jsonString = getJson("movie-review.json")
        return buildDataClassFromJson(jsonString)
    }

    fun getMovieVideoResponse(): Video {
        val jsonString = getJson("movie-video.json")
        return buildDataClassFromJson(jsonString)
    }

    fun getNowPlayingResponse(): BaseModel {
        val jsonString = getJson("now-playing.json")
        return buildDataClassFromJson(jsonString)
    }

    fun getSearchResponse(): BaseModel {
        val jsonString = getJson("search.json")
        return buildDataClassFromJson(jsonString)
    }

    fun getMovieRecommendationResponse(): BaseModel {
        val jsonString = getJson("movie-recommendation.json")
        return buildDataClassFromJson(jsonString)
    }

    fun getMovieCreditResponse(): Artist {
        val jsonString = getJson("movie-credit.json")
        return buildDataClassFromJson(jsonString)
    }

    private fun getJson(resourceName: String): String {
        val file = File("src/test/resources/$resourceName")
        return String(file.readBytes())
    }
}