package ade.test.mandiri.ui.screens.moviedetail

import ade.test.mandiri.data.model.BaseModel
import ade.test.mandiri.data.model.artist.Artist
import ade.test.mandiri.data.model.moviedetail.MovieDetail
import ade.test.mandiri.data.model.video.Video
import ade.test.mandiri.data.repository.MovieRepository
import ade.test.mandiri.utils.network.DataState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(private val repo: MovieRepository) : ViewModel() {
    val movieDetail: MutableState<DataState<MovieDetail>?> = mutableStateOf(null)
    val recommendedMovie: MutableState<DataState<BaseModel>?> = mutableStateOf(null)
    val artist: MutableState<DataState<Artist>?> = mutableStateOf(null)
    val movieVideos : MutableState<DataState<Video>?> = mutableStateOf(null)

    val movieId = MutableStateFlow<Int?>(null)

    @OptIn(ExperimentalCoroutinesApi::class)
    val movieReviews = movieId.flatMapLatest {
        repo.reviewPagingDataSource(it ?: 0)
    }.cachedIn(viewModelScope)

    fun movieDetailApi(movieId: Int) {
        viewModelScope.launch {
            repo.movieDetail(movieId).onEach {
                movieDetail.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun recommendedMovieApi(movieId: Int, page: Int) {
        viewModelScope.launch {
            repo.recommendedMovie(movieId, page).onEach {
                recommendedMovie.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun movieCredit(movieId: Int) {
        viewModelScope.launch {
            repo.movieCredit(movieId).onEach {
                artist.value = it
            }.launchIn(viewModelScope)
        }
    }

    fun movieVideo(movieId: Int) {
        viewModelScope.launch {
            repo.movieVideo(movieId).onEach {
                movieVideos.value = it
            }.launchIn(viewModelScope)
        }
    }
}