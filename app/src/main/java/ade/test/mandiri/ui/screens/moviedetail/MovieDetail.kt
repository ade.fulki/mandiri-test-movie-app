package ade.test.mandiri.ui.screens.moviedetail

import ade.test.mandiri.data.datasource.remote.ApiURL
import ade.test.mandiri.data.model.BaseModel
import ade.test.mandiri.data.model.artist.Artist
import ade.test.mandiri.data.model.moviedetail.MovieDetail
import ade.test.mandiri.data.model.review.ReviewItem
import ade.test.mandiri.data.model.video.Video
import ade.test.mandiri.ui.component.CircularIndeterminateProgressBar
import ade.test.mandiri.ui.component.ExpandingText
import ade.test.mandiri.ui.component.ReviewItemView
import ade.test.mandiri.ui.component.YoutubePlayer
import ade.test.mandiri.ui.component.text.SubtitlePrimary
import ade.test.mandiri.ui.component.text.SubtitleSecondary
import ade.test.mandiri.ui.theme.DefaultBackgroundColor
import ade.test.mandiri.ui.theme.FontColor
import ade.test.mandiri.utils.hourMinutes
import ade.test.mandiri.utils.network.DataState
import ade.test.mandiri.utils.roundTo
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import ade.test.mandiri.R
import ade.test.mandiri.data.model.MovieItem
import ade.test.mandiri.data.model.artist.Cast
import ade.test.mandiri.navigation.Screen
import ade.test.mandiri.ui.theme.cornerRadius
import ade.test.mandiri.utils.pagingLoadingState
import androidx.compose.foundation.lazy.items
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.coil.CoilImage

@Composable
fun MovieDetail(navController: NavController, movieId: Int) {
    val movieDetailViewModel = hiltViewModel<MovieDetailViewModel>()
    val progressBar = remember { mutableStateOf(false) }
    val movieDetail = movieDetailViewModel.movieDetail
    val recommendedMovie = movieDetailViewModel.recommendedMovie
    val artist = movieDetailViewModel.artist
    val videos = movieDetailViewModel.movieVideos

    movieDetailViewModel.movieId.value = movieId
    val reviewsItems: LazyPagingItems<ReviewItem> =
        movieDetailViewModel.movieReviews.collectAsLazyPagingItems()

    LaunchedEffect(true) {
        movieDetailViewModel.movieDetailApi(movieId)
        movieDetailViewModel.recommendedMovieApi(movieId, 1)
        movieDetailViewModel.movieCredit(movieId)
        movieDetailViewModel.movieVideo(movieId)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(
                DefaultBackgroundColor
            )
    ) {
        CircularIndeterminateProgressBar(isDisplayed = progressBar.value, 0.4f)
        movieDetail.value?.let { it ->
            if (it is DataState.Success<MovieDetail>) {
                LazyColumn {
                    item {
                        Column {
                            CoilImage(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(300.dp),
                                imageModel = { ApiURL.IMAGE_URL.plus(it.data.poster_path) },
                                imageOptions = ImageOptions(
                                    contentScale = ContentScale.Crop,
                                    alignment = Alignment.Center,
                                    contentDescription = "Movie detail",
                                    colorFilter = null,
                                ),
                            )
                            Column(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(start = 10.dp, end = 10.dp)
                            ) {
                                Text(
                                    text = it.data.title,
                                    modifier = Modifier.padding(top = 10.dp),
                                    color = FontColor,
                                    fontSize = 30.sp,
                                    fontWeight = FontWeight.W700,
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(bottom = 10.dp, top = 10.dp)
                                ) {

                                    Column(Modifier.weight(1f)) {
                                        SubtitlePrimary(
                                            text = it.data.original_language,
                                        )
                                        SubtitleSecondary(
                                            text = stringResource(R.string.language)
                                        )
                                    }
                                    Column(Modifier.weight(1f)) {
                                        SubtitlePrimary(
                                            text = it.data.vote_average.roundTo(1).toString(),
                                        )
                                        SubtitleSecondary(
                                            text = stringResource(R.string.rating)
                                        )
                                    }
                                    Column(Modifier.weight(1f)) {
                                        SubtitlePrimary(
                                            text = it.data.runtime.hourMinutes()
                                        )
                                        SubtitleSecondary(
                                            text = stringResource(R.string.duration)
                                        )
                                    }
                                    Column(Modifier.weight(1f)) {
                                        SubtitlePrimary(
                                            text = it.data.release_date
                                        )
                                        SubtitleSecondary(
                                            text = stringResource(R.string.release_date)
                                        )
                                    }
                                }
                                Text(
                                    modifier = Modifier.padding(top = 10.dp),
                                    text = stringResource(R.string.description),
                                    color = FontColor,
                                    fontSize = 17.sp,
                                    fontWeight = FontWeight.SemiBold
                                )
                                ExpandingText(text = it.data.overview)

                                Text(
                                    modifier = Modifier.padding(top = 10.dp),
                                    text = stringResource(R.string.video),
                                    color = FontColor,
                                    fontSize = 17.sp,
                                    fontWeight = FontWeight.SemiBold
                                )
                                videos.value?.let { videoDataState ->
                                    if (videoDataState is DataState.Success<Video>) {
                                        YoutubePlayer(
                                            youtubeVideoId = videoDataState.data.results.find { result -> result.site == "YouTube" }?.key.orEmpty(),
                                            lifecycleOwner = LocalLifecycleOwner.current
                                        )
                                    }
                                }


                                recommendedMovie.value?.let {
                                    if (it is DataState.Success<BaseModel>) {
                                        RecommendedMovie(navController, it.data.results)
                                    }
                                }
                                artist.value?.let {
                                    if (it is DataState.Success<Artist>) {
                                        ArtistAndCrew(navController, it.data.cast)
                                    }
                                }
                            }
                        }
                    }
                    item {
                        Text(
                            modifier = Modifier.padding(10.dp),
                            color = FontColor,
                            fontSize = 17.sp,
                            text = stringResource(R.string.review),
                            fontWeight = FontWeight.SemiBold
                        )
                    }
                    items(reviewsItems.itemCount) { index ->
                        reviewsItems[index]?.let {
                            ReviewItemView(it)
                        }
                    }
                }
            }
        }
        recommendedMovie.pagingLoadingState {
            progressBar.value = it
        }
        movieDetail.pagingLoadingState {
            progressBar.value = it
        }
    }
}

@Preview(name = "MovieDetail", showBackground = true)
@Composable
fun Preview() {
    // MovieDetail(null, MovieItem())
}

@Composable
fun RecommendedMovie(navController: NavController?, recommendedMovie: List<MovieItem>) {
    Column(modifier = Modifier.padding(bottom = 10.dp)) {
        Text(
            modifier = Modifier.padding(top = 10.dp),
            text = stringResource(R.string.similar),
            color = FontColor,
            fontSize = 17.sp,
            fontWeight = FontWeight.SemiBold
        )
        LazyRow(modifier = Modifier.fillMaxHeight()) {
            items(recommendedMovie, itemContent = { item ->
                Column(
                    modifier = Modifier.padding(
                        start = 0.dp, end = 8.dp, top = 5.dp, bottom = 5.dp
                    )
                ) {
                    CoilImage(
                        modifier = Modifier
                            .height(190.dp)
                            .width(140.dp)
                            .cornerRadius(10)
                            .clickable {
                                navController?.navigate(
                                    Screen.MovieDetail.route.plus(
                                        "/${item.id}"
                                    )
                                )
                            },
                        imageModel = { ApiURL.IMAGE_URL.plus(item.posterPath) },
                        imageOptions = ImageOptions(
                            contentScale = ContentScale.Crop,
                            alignment = Alignment.Center,
                            contentDescription = "similar movie",
                            colorFilter = null,
                        )
                    )
                }
            })
        }
    }
}

@Composable
fun ArtistAndCrew(navController: NavController?, cast: List<Cast>) {
    Column(modifier = Modifier.padding(bottom = 10.dp)) {
        Text(
            modifier = Modifier.padding(top = 10.dp),
            color = FontColor,
            fontSize = 17.sp,
            text = stringResource(R.string.cast),
            fontWeight = FontWeight.SemiBold
        )
        LazyRow(modifier = Modifier.fillMaxHeight()) {
            items(cast, itemContent = { item ->
                Column(
                    modifier = Modifier.padding(
                        start = 0.dp, end = 10.dp, top = 5.dp, bottom = 5.dp
                    ),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CoilImage(
                        modifier = Modifier
                            .padding(bottom = 5.dp)
                            .height(80.dp)
                            .width(80.dp)
                            .cornerRadius(40)
                            .clickable {
                                navController?.navigate(
                                    Screen.ArtistDetail.route.plus(
                                        "/${item.id}"
                                    )
                                )
                            },
                        imageModel = { ApiURL.IMAGE_URL.plus(item.profilePath) },
                        imageOptions = ImageOptions(
                            contentScale = ContentScale.Crop,
                            alignment = Alignment.Center,
                            contentDescription = "artist and crew",
                            colorFilter = null,
                        )
                    )
                    SubtitleSecondary(text = item.name)
                }
            })
        }
    }
}
