package ade.test.mandiri.ui.screens.home

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import ade.test.mandiri.data.model.GenreId
import ade.test.mandiri.data.model.moviedetail.Genre
import ade.test.mandiri.ui.component.MovieItemList

@Composable
fun HomeScreen(
    navController: NavController,
    genres: ArrayList<Genre>? = null,
) {
    val homeViewModel = hiltViewModel<HomeViewModel>()
    MovieItemList(
        navController = navController,
        movies = homeViewModel.nowPlayingMovies,
        genres = genres,
        selectedName = homeViewModel.selectedGenre.value
    ){
        homeViewModel.filterData.value = GenreId(it?.id.toString())
        it?.let {
            homeViewModel.selectedGenre.value = it
        }
    }
}