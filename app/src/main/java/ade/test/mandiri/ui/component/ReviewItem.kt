package ade.test.mandiri.ui.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import ade.test.mandiri.data.model.review.ReviewItem
import ade.test.mandiri.ui.theme.cornerRadius


@Composable
fun ReviewItemView(item: ReviewItem) {
    Card (
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .cornerRadius(4),
        elevation = 8.dp
    ) {
        Column(modifier = Modifier.padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 10.dp)) {
            Text(
                text = item.author,
                fontSize = 14.sp,
                fontWeight = FontWeight.Bold
            )
            Text(
                text = "Rating : " + item.authorDetails.rating.toString(),
                fontSize = 12.sp,
                fontWeight = FontWeight.Light
            )
            Text(
                modifier = Modifier.padding(top = 5.dp),
                text = item.content,
                fontSize = 10.sp,
                fontWeight = FontWeight.Light
            )
        }
    }
}

