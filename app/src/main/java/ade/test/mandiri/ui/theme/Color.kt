package ade.test.mandiri.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFF90CAF9)
val Purple500 = Color(0xFF2196F3)
val Purple700 = Color(0xFF1976D2)
val Teal200 = Color(0xFFB0BEC5)
val FontColor = Color(0xFF212121)
val SecondaryFontColor = Color(0xFF757575)
val DefaultBackgroundColor = Color(0xFFFAFAFA)
val Blue = Color(0xff76a9ff)
val FloatingActionBackground = Color(0xffFBC02D)
val LinkColor = Color(0xff64B5F6)