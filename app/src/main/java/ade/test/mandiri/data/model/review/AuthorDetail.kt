package ade.test.mandiri.data.model.review

import com.google.gson.annotations.SerializedName

data class AuthorDetail(
    @SerializedName("name")
    val name: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("avatar_path")
    val avatarPath: String,
    @SerializedName("rating")
    val rating: Int
)