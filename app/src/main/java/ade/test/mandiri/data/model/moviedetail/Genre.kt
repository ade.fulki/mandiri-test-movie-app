package ade.test.mandiri.data.model.moviedetail

data class Genre(
    val id: Int?,
    val name: String
)