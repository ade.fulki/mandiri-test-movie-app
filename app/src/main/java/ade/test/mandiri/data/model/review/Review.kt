package ade.test.mandiri.data.model.review

import com.google.gson.annotations.SerializedName

data class Review(
    @SerializedName("id") 
    val id: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: ArrayList<ReviewItem>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)