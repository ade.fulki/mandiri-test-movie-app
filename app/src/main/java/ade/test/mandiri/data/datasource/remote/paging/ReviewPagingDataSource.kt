package ade.test.mandiri.data.datasource.remote.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import ade.test.mandiri.data.datasource.remote.ApiService
import ade.test.mandiri.data.model.review.ReviewItem
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class ReviewPagingDataSource @Inject constructor(private val apiService: ApiService, private val movieId: Int) :
    PagingSource<Int, ReviewItem>() {

    override fun getRefreshKey(state: PagingState<Int, ReviewItem>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ReviewItem> {
        return try {
            val nextPage = params.key ?: 1
            val movieList = apiService.movieReview(movieId, nextPage)
            LoadResult.Page(
                data = movieList.results,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey =  if (movieList.results.isNotEmpty()) movieList.page + 1 else  null
            )
        } catch (exception: IOException) {
            Timber.e("exception ${exception.message}")
            return LoadResult.Error(exception)
        } catch (httpException: HttpException) {
            Timber.e("httpException ${httpException.message}")
            return LoadResult.Error(httpException)
        }
    }
}