package ade.test.mandiri.data.repository

import androidx.paging.PagingData
import ade.test.mandiri.data.model.BaseModel
import ade.test.mandiri.data.model.Genres
import ade.test.mandiri.data.model.MovieItem
import ade.test.mandiri.data.model.artist.Artist
import ade.test.mandiri.data.model.artist.ArtistDetail
import ade.test.mandiri.data.model.moviedetail.MovieDetail
import ade.test.mandiri.data.model.review.ReviewItem
import ade.test.mandiri.data.model.video.Video
import ade.test.mandiri.utils.network.DataState
import kotlinx.coroutines.flow.Flow

interface MovieRepositoryInterface {
    suspend fun movieDetail(movieId: Int): Flow<DataState<MovieDetail>>
    suspend fun recommendedMovie(movieId: Int, page: Int): Flow<DataState<BaseModel>>
    suspend fun search(searchKey: String): Flow<DataState<BaseModel>>
    suspend fun genreList(): Flow<DataState<Genres>>
    suspend fun movieCredit(movieId: Int): Flow<DataState<Artist>>
    suspend fun movieVideo(movieId: Int): Flow<DataState<Video>>
    suspend fun artistDetail(personId: Int): Flow<DataState<ArtistDetail>>
    fun nowPlayingPagingDataSource(genreId: String?): Flow<PagingData<MovieItem>>
    fun genrePagingDataSource(genreId: String): Flow<PagingData<MovieItem>>
    fun reviewPagingDataSource(movieId: Int): Flow<PagingData<ReviewItem>>
}