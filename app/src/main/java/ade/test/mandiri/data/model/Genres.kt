package ade.test.mandiri.data.model

import ade.test.mandiri.data.model.moviedetail.Genre

data class Genres(val genres: List<Genre>)