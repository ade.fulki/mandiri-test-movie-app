package ade.test.mandiri

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class HiltApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        if (ade.test.mandiri.BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}